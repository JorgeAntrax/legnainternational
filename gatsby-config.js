module.exports = {
	siteMetadata: {
		siteUrl: "https://legnainternational.com",
		title: "Legna International Trade",
		author: "Digital Booting a Creative Digital Agency",
		description:
			"Our team of experts is your comprehensive guide to successful projects from start to finish. We deliver premium results with environmentally sound solutions.",
	},
	plugins: [
		"gatsby-plugin-sass",
		"gatsby-plugin-styled-components",
		{
			resolve: `gatsby-omni-font-loader`,
			options: {
				enableListener: true,
				preconnect: [
					`https://fonts.googleapis.com`,
					`https://fonts.gstatic.com`,
				],
				web: [
					{
						name: `Poppins`,
						file: `https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;800&display=swap`,
					},
				],
			},
		},
		"gatsby-plugin-image",
		"gatsby-transformer-remark",
		"gatsby-plugin-mdx",
		"gatsby-plugin-sharp",
		"gatsby-transformer-sharp",
		{
			resolve: "gatsby-plugin-remove-console",
			options: {
				exclude: ["error", "info"],
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `assets`,
				path: `${__dirname}/src/assets/`,
			},
		},
		{
			resolve: "gatsby-source-filesystem",
			options: {
				name: "pages",
				path: "./src/pages/",
			},
			__key: "pages",
		},
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				start_url: `/`,
				name: `Legna International Trade`,
				short_name: `LIT`,
				background_color: `#ffffff`,
				display: `minimal-ui`,
				icon: `src/assets/icon.png`, // This path is relative to the root of the site.
			},
		},
	],
};
