const path = require('path');
exports.onCreateWebpackConfig = ({ stage, actions }) => {
	actions.setWebpackConfig({
		resolve: {
			alias: {
				"@src": path.resolve(__dirname,"src"),
				"@seo": path.resolve(__dirname,"src/seo"),
				"@middlewares": path.resolve(__dirname,"src/middlewares"),
				"@components": path.resolve(__dirname,"src/components"),
				"@modules": path.resolve(__dirname,"src/modules"),
				"@constants": path.resolve(__dirname,"src/constants"),
				"@containers": path.resolve(__dirname,"src/containers"),
				"@context": path.resolve(__dirname,"src/context"),
				"@layout": path.resolve(__dirname,"src/layout"),
				"@assets": path.resolve(__dirname,"src/assets"),
				"@services": path.resolve(__dirname,"src/services"),
				"@utils": path.resolve(__dirname,"src/utils"),
				"@hooks": path.resolve(__dirname,"src/hooks"),
				"@states": path.resolve(__dirname,"src/states")
			}
		}
	})
}
