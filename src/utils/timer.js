import { MSDATE } from "@constants/config";
const { MS_DAYS, MS_HOURS, MS_MINUTES, MS_SECONDS } = MSDATE;

export default (key) => {
	let lastActivity = sessionStorage.getItem(key);
	lastActivity = new Date(+lastActivity);

	if (lastActivity) {
		const currentActivity = new Date();
		const DIFFERENCE = currentActivity - lastActivity;
		return {
			hours: Math.floor((DIFFERENCE % MS_DAYS) / MS_HOURS),
			minutes: Math.floor((DIFFERENCE % MS_HOURS) / MS_MINUTES),
			seconds: Math.floor((DIFFERENCE % MS_MINUTES) / MS_SECONDS),
		};
	} else {
		return {
			hours: 0,
			minutes: 0,
			seconds: 0,
		};
	}
};
