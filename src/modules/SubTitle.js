import React from "react"
import { Text } from "@components"

const SubTitle = ({ children, invert, fs, ...props }) => {
	return (
		<Text color={!invert ? "#1195cc" : "#A7D05D"} fs={fs || 35} {...props}>
			{children}
		</Text>
	)
}

export default SubTitle
