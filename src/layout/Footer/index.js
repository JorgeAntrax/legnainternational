import React from "react"
import { Flex, Svg, Text } from "@components"
import { SectionBlue, SectionGrey } from "./styles"
import logo from "@assets/logo.svg"
import twitter from "@assets/icons/TwitterLogo.svg"
import insta from "@assets/icons/InstagramLogo.svg"
import fb from "@assets/icons/FacebookLogo.svg"

const Footer = () => {
	return (
		<Flex>
			<SectionBlue>
				<Flex align="center" justify="between">
					<Svg xs="200:50" icon={logo} />

					<span>
						<Flex align="center" justify="center">
							<Text fs={14} align="center">
								<i>401 E. Main Suite 412 El Paso Tx 79901</i>
							</Text>
							<Text fs={14} align="center">
								<i>aalanis@legnainternational.com</i>
							</Text>
						</Flex>
					</span>
				</Flex>
			</SectionBlue>
			<SectionGrey>
				<Flex align="center" justify="between">
					<span>
						<Text inline fs={12} fw={500} align="center">
							FOLLOW
						</Text>
					</span>
					<span>
						<Flex align="center" justify="between">
							<Svg xs="24:24" className="mr:1" icon={twitter} />
							<Svg xs="24:24" className="mr:1" icon={insta} />
							<Svg xs="24:24" className="" icon={fb} />
						</Flex>
					</span>
				</Flex>
			</SectionGrey>
		</Flex>
	)
}

export default Footer
