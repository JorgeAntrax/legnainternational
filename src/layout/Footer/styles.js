import styled from "styled-components"

const SectionBlue = styled.div`
	padding: 1rem 2rem;
	background-color: #1195cc;
	color: white;
	flex: 3;
`
const SectionGrey = styled.div`
display: flex;
align-items: center;
	padding: 1rem 2rem;
	background-color: #d6d6d6;
	color: black;
	flex: 1;
`

export { SectionBlue, SectionGrey }
