import React from "react"

import Navbar from "@layout/Navbar"
import Footer from "@layout/Footer"
import { Flex, Modal, Text, Loading } from "@components"

const Layout = ({ children, navbarTranslucent = false }) => {
	return (
		<>
			<Navbar translucent={navbarTranslucent} />
			{children}
			<Footer />
		</>
	)
}

export default Layout
