import st, { css } from "styled-components"

const NavWrapper = st.div`
	display: flex;
	width: 100%;
	justify-content: space-between;
	z-index: 10;

	padding: ${({ translucent }) => (translucent ? "2rem" : "0.5rem 2rem")};
	transition: all 200ms ease;
	position: fixed;
	top: 0;

	${({ translucent }) =>
		translucent &&
		css`
			background: rgb(0, 0, 0);
			background: linear-gradient(
				180deg,
				rgba(0, 0, 0, 1) 0%,
				rgba(0, 0, 0, 0) 100%
			);
		`}
	${({ translucent }) =>
		!translucent &&
		css`
			background: #060606;
		`}
`

const IconButton = st.button`
	width: 40px;
	height: 40px;
	min-width: 40px;
	min-height: 40px;
	border-radius: 40px;
	display: inline-flex;
	align-items: center;
	align-content: center;
	justify-content: center;
	cursor: pointer;

	&:hover {
		background-color: #E8EFFF;
	}
`

const NavigationBar = st.div`
	display: inline-flex;
	background-color: white;
	border-radius: 40px;
	align-items: center;
	align-content: center;
	justify-content: end;
	padding: 0.25rem 0.5rem;
`

const NavSearch = st.div`
	min-width: 200px;
	width: 200px;
	display: inline-flex;

	div {
		border-radius: 40px;
		margin-right: 0.5rem
	}
`

export { NavWrapper, IconButton, NavigationBar, NavSearch }
