import React, { useEffect, useState } from "react"
import { Flex, Col, Svg, Text, Input } from "@components"

import { NavWrapper, IconButton, NavigationBar, NavSearch } from "./styles"
import { Link, navigate } from "gatsby"
import { useWidth } from "@hooks"

import logotipo from "@assets/logo.svg"
import notify from "@assets/icons/notify.svg"
import moon from "@assets/icons/moon.svg"

const Navbar = ({ translucent }) => {
	const { isMobile } = useWidth()
	const [route, setRoute] = useState("")
	const [noTranslucentStyleScrolling, removeTranslucent] = useState(translucent)

	useEffect(() => {
		if (typeof window !== "undefined") {
			setRoute(window.location.pathname.split("/").join(" / "))

			window.onscroll = function () {
				var y = window.scrollY
				console.log(y)

				if (y > 250) removeTranslucent(false)
				if (y < 250) removeTranslucent(translucent)
			}
		}
	}, [])

	return (
		<NavWrapper translucent={noTranslucentStyleScrolling}>
			<Flex align="center" justify="between">
				<Col autofit>
					<a href="/">
						<Svg xs="300:60" icon={logotipo} />
					</a>
				</Col>
				<Col
					autofit
					className="t:right"
					style={{ width: isMobile ? "100%" : "auto" }}
				>
					<Flex
						align="center"
						justify={!isMobile && "end"}
						style={{
							width: isMobile ? "100%" : "auto",
							overflowX: isMobile ? "auto" : "hidden",
							overflowY: "hidden",
							padding: isMobile ? "1rem" : "0",
						}}
						wrapper
					>
						<a href="/about">
							<Text
								color="white"
								fs={16}
								fw={300}
								className="ph:1"
								transform="uppercase"
								link
							>
								About
							</Text>
						</a>
						<a href="/services">
							<Text
								color="white"
								fs={16}
								fw={300}
								className="ph:1"
								transform="uppercase"
								link
							>
								Services
							</Text>
						</a>
						<a href="/portafolio">
							<Text
								color="white"
								fs={16}
								fw={300}
								className="ph:1"
								transform="uppercase"
								link
							>
								Portafolio
							</Text>
						</a>
						{/* <a href="/news">
							<Text
								color="white"
								fs={16}
								fw={300}
								className="ph:1"
								transform="uppercase"
								link
							>
								News
							</Text>
						</a> */}
						<a href="/contact">
							<Text
								color="white"
								fs={16}
								fw={300}
								className="ph:1"
								transform="uppercase"
								link
							>
								Contact
							</Text>
						</a>
					</Flex>
				</Col>
			</Flex>
		</NavWrapper>
	)
}

export default Navbar
