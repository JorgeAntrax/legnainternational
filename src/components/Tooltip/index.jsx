import React from "react";
import { WrappTooltip, TooltipContent } from "./styles";
import { Svg, Center } from "@components";

import tooltip from "@assets/icons/info.svg";

const icons = {
	info: tooltip,
};

export default ({
	element,
	children,
	w,
	direction,
	delay = 150,
	icon,
	widthIcon = 20,
}) => {
	const fileIcon = icons[icon];
	return (
		<WrappTooltip>
			{element && element}
			{icon && <Svg icon={fileIcon} width={widthIcon} height={widthIcon} />}
			{children && (
				<TooltipContent
					className="content"
					delay={delay}
					direction={direction || "top"}
					w={w || 200}
				>
					<small style={{ fontWeight: 300 }}>{children}</small>
				</TooltipContent>
			)}
		</WrappTooltip>
	);
};
