import React from "react"
import st, { css } from "styled-components"
import { switchProp } from "styled-tools"

const switchJustify = switchProp("justify", {
	around: "justify-content: space-around;",
	between: "justify-content: space-between;",
	start: "justify-content: start;",
	center: "justify-content: center;",
	end: "justify-content: end;",
})

const switchAlign = switchProp("align", {
	around: "align-items: space-around;",
	between: "align-items: space-between;",
	start: "align-items: start;",
	center: "align-items: center; align-content: center;",
	end: "align-items: end;",
})

const FlexWrapper = st.div`
	display: ${({ inline }) => (inline ? "inline-flex" : "flex")};
	flex-wrap: ${({ wrapper }) => (!wrapper ? "wrap" : "nowrap")};
	width: ${({ inline, width }) => (inline ? "auto" : width || "100%")};
	max-width: ${({ maxWidth }) => maxWidth || "100%"};
	${({ align }) => (align ? switchAlign : "")}
	${({ justify }) => (justify ? switchJustify : "")}


	${({ xsWidth }) =>
		xsWidth &&
		css`
			@media screen and (min-width: 320px) {
				width: ${({ inline, xsWidth }) =>
					inline ? "auto" : xsWidth || "100%"};
				max-width: ${({ xsMaxWidth }) => xsMaxWidth || "100%"};
			}
		`}
	${({ mdWidth }) =>
		mdWidth &&
		css`
			@media screen and (min-width: 768px) {
				width: ${({ inline, mdWidth }) =>
					inline ? "auto" : mdWidth || "100%"};
				max-width: ${({ mdMaxWidth }) => mdMaxWidth || "100%"};
			}
		`}
	${({ mdWrapper }) =>
		mdWrapper &&
		css`
			@media screen and (min-width: 768px) {
				flex-wrap: ${({ mdWrapper }) => (mdWrapper ? "nowrap" : "wrap")};
			}
		`}
`

export default ({ children, ...props }) => {
	return <FlexWrapper {...props}>{children}</FlexWrapper>
}
