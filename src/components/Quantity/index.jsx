import React, { useState, useEffect } from 'react'
import { Svg } from "@components"
import {
	Label,
	Button,
	Quantity,
	QuantityWrapper
} from './styles'

import aleft from "@assets/icons/aleft.svg"
import aright from "@assets/icons/aright.svg"
import rollback from "@assets/icons/rollback.svg"

export default ({children, max = 10, step = 1, getValue, value = 0, ...props}) => {
	const [count, setCount] = useState(1);

	const rest = () => {
		const v = count - step > 0 ? count - step : step;
		setCount(v);
	}

	const add = () => {
		const v = count + step < max ? count + step : max;
		setCount(v);
	}

	const reset = () => setCount(1)
	useEffect(() => {
		getValue && getValue(count)
	}, [count])

	useEffect(() => {
		if(!!value) {
			setCount(value)
		}
	}, [])

	return (
		<Quantity {...props}>
			{!!children && (<Label style={{ paddingLeft: '0.8rem', paddingRight: '0.8rem' }}>{children}</Label>)}
			<QuantityWrapper>
				<Button onClick={() => rest()}>
					<Svg icon={aleft} width={20} height={20} />
				</Button>
				<Label>{count}</Label>
				<Button onClick={() => add()}>
					<Svg icon={aright} width={20} height={20} />
				</Button>
				{
					count > 1 && (
						<Button onClick={() => reset()} style={{ right: '-2rem' }}>
							<Svg icon={rollback} width={20} height={20} />
						</Button>
					)
				}
			</QuantityWrapper>
		</Quantity>
	)
}
