import React, { useState, useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import InputMask from "react-input-mask";
import {
	WrappInput,
	ItemInput,
	Label,
	ErrorMessage,
	IconFinger,
} from "./styles";
import { REGEX } from "@constants";

import { Svg } from "@components";
import fingerIcon from "@assets/icons/finger.svg";
import keyIcon from "@assets/icons/key.svg";
import rollback from "@assets/icons/rollback.svg";

const Textarea = ({
	placeholder,
	type,
	getValue,
	mask,
	value,
	forwardRef,
	onKeyDown,
	onKeyUp,
	onChange,
	onFocus,
	onCopy,
	onPaste,
	error,
	disabled,
	maxLength,
	isNumber,
	scapeCharacters,
	onlyCharaters,
	onlyAlpha,
	isCurp,
	onlyNL,
	onlyPositiveNumbers,
	readonly,
	capitalize,
}) => {
	const [hasFocus, setHasFocus] = useState(false);
	const [localError, setLocalError] = useState("");
	const [showPasswd, setShowPasswd] = useState(false);
	const { emailRgx, curpRgx, specialChars, onlyLetterNumbers } = REGEX;

	const handleChange = (e) => {
		const { type } = e.target;
		let { value } = e.target;

		if (onlyAlpha) {
			value = value.replace(
				/[0-9$&\\+:·,.;=¬?´@¿¡ç#|'"/`|°¨<>_^*()%![\]}+~{-]/g,
				""
			);
		}

		if (scapeCharacters) {
			value = value.replace(specialChars, "");
		}

		if (onlyCharaters) {
			//permite guion
			value = value.replace(
				/[$&\\+:·,;=¬?´@¿¡ç#|'"/`|°¨<>_^*()%![\]}+~{]/g,
				""
			);
		}

		if (onlyNL) {
			value = value.replace(onlyLetterNumbers, "");
		}

		if (onlyPositiveNumbers) {
			const valid = /^[1-9]$|^[1-9][0-9]$|^(100)$/.test(value);
			if (!valid) value = value.slice(0, -1);
		}

		if (type === "email") {
			const valid = emailRgx.test(value);
			setLocalError(valid || value.length === 0 ? "" : "Formato no válido");
		}

		if (isCurp) {
			const valid = curpRgx.test(value);
			setLocalError(valid || value.length === 0 ? "" : "Curp no válido");
		}

		if (capitalize) {
			value = value
				.toLowerCase()
				.replace(/\w/, (firstLetter) => firstLetter.toUpperCase());
		}

		onChange && onChange(value);
		if (getValue) {
			maxLength
				? getValue(value.length > maxLength ? value.slice(0, maxLength) : value)
				: getValue(value);
		}
	};

	const handleFocus = () => {
		onFocus && onFocus();
		setHasFocus(true);
	};

	const handleCopy = (e) => {
		if (onCopy) e.preventDefault();
	};
	const handlePaste = (e) => {
		if (onPaste) e.preventDefault();
	};

	return (
		<WrappInput
			small={!placeholder}
			hasError={error.length || localError.length}
			disabled={disabled}
		>
			<ItemInput
				readonly={readonly}
				as={InputMask ? mask : null}
				mask={mask ? mask : null}
				onFocus={() => handleFocus()}
				onBlur={() => setHasFocus(false)}
				value={value}
				ref={forwardRef}
				onChange={handleChange}
				onKeyDown={onKeyDown}
				onKeyUp={onKeyUp}
				onPaste={handlePaste}
				onCopy={handleCopy}
				disabled={disabled}
				maxLength={maxLength}
				min={isNumber ? "0" : null}
				max={isNumber ? "9" : null}
				autocomplete="off"
			/>

			{!!value && (
				<IconFinger className="actionButton" style={{ right: type === "password" ? "2.5rem" : "0.5rem" }}>
					<Svg
						onClick={() => getValue("")}
						icon={rollback}
						width={20}
						height={20}
					/>
				</IconFinger>
			)}
			{placeholder && (
				<Label
					hasFocus={hasFocus || (!!value && value.length)}
					disabled={disabled}
				>
					{placeholder}
				</Label>
			)}
			{error && <ErrorMessage>{error}</ErrorMessage>}
			{localError && <ErrorMessage>{localError}</ErrorMessage>}
		</WrappInput>
	);
};

Textarea.defaultProps = {
	error: "",
};

Textarea.propTypes = {
	placeholder: PropTypes.string,
	getValue: PropTypes.func.isRequired,
	mask: PropTypes.string,
	value: PropTypes.string.isRequired,
	disabled: PropTypes.bool,
	forwardRef: PropTypes.oneOfType([
		PropTypes.func,
		PropTypes.shape({ current: PropTypes.any }),
	]),
	onKeyDown: PropTypes.func,
	onKeyUp: PropTypes.func,
	onChange: PropTypes.func,
	onFocus: PropTypes.func,
	error: PropTypes.string,
};

export default Textarea;
