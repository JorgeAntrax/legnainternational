import { useLocation } from "@reach/router";

export default function useQuery() {
	return new URLSearchParams(useLocation().search);
}
