import { useEffect, useState } from "react";

export default (category, getValue, type = 'checkbox') => {
	const [showOtro, setShowOtro] = useState(false);
	const [data, setData] = useState([]);

	useEffect(() => {
		const isValid = !!data.length;
		getValue && getValue({
			type: category,
			isValid,
			data: { [category]: data.join(',') }
		});
	}, [data]);

	const handleChangeData = ({ checked, value }) => {
		console.log(checked, value);
		if (checked) {
			if (value !== "otro") {
				if(type === 'radio') {
					setData([value]);
				}else {
					setData([...data, value]);
				}
			} else {
				setShowOtro(true);
			}
		} else {
			if (value !== "otro") {
				const _data = data.filter((el) => el !== value);
				setData([..._data]);
			} else {
				setShowOtro(false);
			}
		}
	};

	const handleChange = (data) => {
		setShowOtro(false);
		setTimeout(() => {
			if(data === null) return;
			handleChangeData(data);
		}, 500);
	};

	return {
		data,
		showOtro,
		handleChangeData,
		handleChange,
	};
};
