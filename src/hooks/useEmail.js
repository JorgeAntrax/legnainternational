// import nodemailer from "nodemailer"
import showdown from "showdown"

const getUserNotifyTemplate = data => {
	const converter = new showdown.Converter()
	const template = `
		<p align="center">
			<img width=200 height=100 src="https://legnainternational.com/assets/logo.png">
		</p>

		# Hola ${data.name}

		Gracias por ponerte en contacto con nosotros, en breve nuestro equipo de expertos le dará seguimiento a tu **solicitud!**

		tienes dudas o sugerencias, envíanos un [email](mailto:info@legnainternational.com).

		> **IA Legna Intenational:** Este **email** 2023.
	`
	return converter.makeHtml(template)
}

const getAdminNotifyTemplate = data => {
	const converter = new showdown.Converter()
	const template = `
		<p align="center">
			<img width=200 height=100 src="https://legnainternational.com/assets/logo.png">
		</p>

		# Se ha capturado un nuevo lead desde el formulario

		## Datos del usuario:
		- ${data.name}
		- ${data.email}
		- ${data.message}

		Dale seguimiento a su solicitud para fidelizar a tus clientes.
		tienes dudas o sugerencias sobre el desarrollo comercial de tu sitio web, envíanos un [email](mailto:leonardo@digitalbooting.com).

		> **IA Digital Booting:** A Creative Digital **Agency** 2023.
	`
	return converter.makeHtml(template)
}

const useEmail = () => {
	const sendEmail = data => {
		const message = `
			Hola, se ha generado un nuevo lead en la página web legnainternational.com con la sigueinte información:%0D%0A
			%0D%0A
			- ${data.name}%0D%0A
			- ${data.email}%0D%0A
			- ${data.message}%0D%0A
			%0D%0A
			Fideliza a tus prospectos, brindandoles una rapida atención.%0D%0A
			%0D%0A
			- IA Legna Interntional Trade
		`

		window.location.href = `https://wa.me/+19158817011/?text=${message}`
	}

	return sendEmail
}

export default useEmail
