import React from "react"
import { Text, Button, Flex } from "@components"
import { SubTitle } from "@modules"
import { navigate } from "gatsby"

const Services = () => {
	return (
		<Flex
			className="pv:5 ph:2"
			align="center"
			justify="center"
			style={{ backgroundColor: "black", color: "white" }}
		>
			<Flex mdWidth="60%">
				<SubTitle invert>SERVICES</SubTitle>
				<Text fs={18} fw={300} className="mv:2">
					<ul>
						<li>PROPERTY IMPROVEMENT PLAN ADMINISTRATION</li>
						<li>PROCUREMENT: FF&E / OS&E</li>
						<li>CONSTRUCTION PROJECT MANAGEMENT</li>
						<li>ENERGY MANAGEMENT</li>
					</ul>
				</Text>

				<Button square ligth onClick={() => navigate('/services')}>Learn More &gt;</Button>
			</Flex>
		</Flex>
	)
}

export default Services
