import React, { useState, useEffect } from "react"
import { Text, Svg, Button, Flex } from "@components"

import left from "@assets/icons/aleft.svg"
import right from "@assets/icons/aright.svg"

const sliders = [
	{
		text: (
			<Text
				fs={20}
				fw={300}
				className="mv:2"
				align="right"
				style={{ fontStyle: "italic" }}
			>
				<i>
					"We evaluate our contractors based upon financial performance,
					honesty, quality, on time performance, and good communication. In our
					experience, LIT ranks highly on each of these categories. We recommend
					LIT without reservation."
				</i>
			</Text>
		),
		author: (
			<Text
				fs={14}
				fw={300}
				className="mb:2"
				align="right"
				style={{ fontStyle: "italic" }}
			>
				<cite>- Button Capital Limited</cite>
			</Text>
		),
	},
]

const SlideFooter = () => {
	const [position, setPosition] = useState(0)

	const handlePosition = operation => {
		const limit = sliders.length - 1
		let count = position
		if (operation === "-" && count > 0) {
			count--
		}

		if (operation === "+" && count < limit) {
			count++
		}

		setPosition(count)
	}
	return null

	return (
		<Flex
			className="pv:5 ph:2"
			align="center"
			justify="center"
			style={{ backgroundColor: "#A7D05D", color: "black" }}
		>
			{sliders.length > 1 && (
				<Svg
					xs="40:40"
					icon={left}
					onClick={() => handlePosition("-")}
					style={{ cursor: "pointer" }}
				/>
			)}

			<Flex width="60%" align="center" justify="center" className="ph:2">
				{!!sliders.length && (
					<>
						{sliders[position].text}
						{sliders[position].author}
					</>
				)}
			</Flex>

			{sliders.length > 1 && (
				<Svg
					xs="40:40"
					icon={right}
					onClick={() => handlePosition("+")}
					style={{ cursor: "pointer" }}
				/>
			)}
		</Flex>
	)
}

export default SlideFooter
