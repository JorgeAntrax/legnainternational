export { default as Banner } from './Banner';
export { default as About } from './About';
export { default as Services } from './Services';
export { default as Form } from './Form';
export { default as Slide } from './Slide';
