import React, { useEffect, useState } from "react"
import { Wrapper } from "./styles"

import image from "@assets/proyects/lobby/IMG_8251.jpg"
import portafolio from "@assets/portafolio.jpg"
import about from "@assets/about.jpg"

import { Title, Text, Flex } from "@components"
import { useWidth } from "@hooks"

const images = [portafolio, image, about]

const Banner = () => {
	const { isMobile } = useWidth()

	const [currentImage, setCurrentImage] = useState(image);
	const [count, setCount] = useState(null);

	useEffect(() => {
		setCount(0);
	}, []);

	useEffect(() => {
		setTimeout(() => {
			setCurrentImage(images[count]);
			setCount(count === 0 ? 1 : count === 1 ? 2 : 0);
		}, 5000);
	}, [count]);


	return (
		<Wrapper image={currentImage} height={700}>
			<Flex align="center" justify="center" style={{ height: "100%" }}>
				<Title
					xs={40}
					md={60}
					fs={60}
					ls="4px"
					maxWidth="450px"
					align={isMobile && "center"}
				>
					Legna International Trade
				</Title>
				<Text fs={22} align="center" fw={300} className="mt:2" ls="2px">
					Professional Procurement Agency
				</Text>
			</Flex>
		</Wrapper>
	)
}

export default Banner
