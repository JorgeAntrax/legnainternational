import styled from "styled-components"

const Wrapper = styled.div`
	background-attachment: fixed;
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	background-image: url(${({ image }) => image});
	height: ${({ height }) => height || 0}px;
	width: 100%;
	overflow: hidden;
	color: white;

	transition: all 400ms linear;
`

export { Wrapper }
