import React, { useState, useEffect } from "react"

import { Flex, Col, Text, Input, Textarea, Button } from "@components"
import { SubTitle } from "@modules"
import { Wrapper } from "./styles"

import map from "@assets/map.jpg"

import { useEmail, useWidth } from "@hooks"
import { REGEX } from "@constants"

const Form = () => {
	const { isMobile } = useWidth()
	const sendEmail = useEmail()
	const [data, setData] = useState({
		name: "",
		email: "",
		message: "",
	})

	const [enable, setEnable] = useState(false)

	const update = (key, value) => {
		setData({
			...data,
			[key]: value,
		})
	}

	useEffect(() => {
		setEnable(
			data.name.length > 3 &&
				data.message.length &&
				REGEX.emailRgx.test(data.email)
		)
	}, [data])

	return (
		<Flex>
			<Col md="10">
				<Flex
					className="p:2"
					align="center"
					justify="center"
					style={{ height: "100%" }}
				>
					<Flex maxWidth="500px">
						<SubTitle>Contact us!</SubTitle>
						<Text className="mv:2" fs={18} fw={300}>
							Our team of experts is eager to help and ready to execute valuable
							experiences and sound results.
						</Text>

						<Input
							value={data.name}
							getValue={v => update("name", v)}
							className="mb:1"
							placeholder="Name"
						/>
						<Input
							value={data.email}
							getValue={v => update("email", v)}
							className="mb:1"
							placeholder="Email"
						/>
						<Textarea
							value={data.message}
							getValue={v => update("message", v)}
							className="mb:1"
							placeholder="Message"
						/>
						<Button
							disabled={!enable}
							square
							block
							className="mv:2"
							onClick={() => sendEmail(data)}
						>
							send
						</Button>

						<Flex mdWrapper justify="between" align="center">
							<Text fs={12} fw={300} align={isMobile ? "center" : "left"}>
								401 E. Main Suite 412 El Paso Tx 79901
							</Text>
							<Text fs={12} fw={300} align={isMobile ? "center" : "right"}>
								aalanis@legnainternational.com
							</Text>
						</Flex>
					</Flex>
				</Flex>
			</Col>
			<Col md="10">
				<a
					href="https://www.bing.com/maps?osid=828cf8ad-730c-4933-a83f-9da4cc8d0007&cp=31.840164~-106.537073&lvl=16&v=2&sV=2&form=S00027"
					target="_blank"
				>
					<Wrapper image={map} height={600} />
				</a>
			</Col>
		</Flex>
	)
}

export default Form
