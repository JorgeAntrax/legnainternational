import styled from "styled-components";

const Wrapper = styled.div`
	background-repeat: no-repeat;
	background-position: center;
	background-image: url(${({ image }) => image});
	height: ${({ height }) => height || 0}px;
	width: 100%;
	overflow: hidden;
`

export { Wrapper }
