import React from "react"
import { Text, Button, Flex } from "@components"
import { SubTitle } from "@modules"

import { navigate } from "gatsby"

const About = () => {
	return (
		<Flex className="pv:5 ph:2" align="center" justify="center">
			<Flex mdWidth="60%">
				<SubTitle>ABOUT US</SubTitle>
				<Text fs={18} fw={300} className="mv:2">
					Our team of experts is your comprehensive guide to successful projects
					from start to finish. We deliver premium results with environmentally
					sound solutions.
				</Text>

				<Button square ligth onClick={() => navigate('/about')}>Learn More &gt;</Button>
			</Flex>
		</Flex>
	)
}

export default About
