import styled from "styled-components"

const Wrapper = styled.div`
	background-attachment: fixed;
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	background: url(${({ image }) => image}), rgba(0, 0, 0, 0.8);
	height: ${({ height }) => height || 0}px;
	width: 100%;
	overflow: hidden;
	color: white;
`

export { Wrapper }
