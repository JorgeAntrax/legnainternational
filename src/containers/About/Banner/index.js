import React from "react"
import { Wrapper } from "./styles"
import img from "@assets/about.jpg"

import { Title, Text, Flex } from "@components"

const Banner = ({ title, image }) => {
	return (
		<Wrapper image={image || img} height={400}>
			<Flex align="center" justify="center" style={{ height: "100%" }}>
				<Title xs={40} fs={60} maxWidth="500px" ls="4px" align="center">
					{title || "ABOUT US!"}
				</Title>
				{/* <Text fs={22} align="center" fw={300} className="mt:2" ls="2px">Professional Procurement Agency</Text> */}
			</Flex>
		</Wrapper>
	)
}

export default Banner
