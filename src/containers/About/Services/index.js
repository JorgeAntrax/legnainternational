import React from "react"
import { Text, Button, Flex } from "@components"
import { SubTitle } from "@modules"

const Services = ({ showServices }) => {
	return (
		<Flex
			className="pv:5 ph:2"
			align="center"
			justify="center"
			style={{
				backgroundColor: !showServices ? "#1195CC" : "white",
				color: !showServices ? "white" : "black",
			}}
		>
			<Flex mdWidth="60%">
				{!showServices && (
					<>
						<SubTitle invert>HISTORY</SubTitle>
						<Text fs={18} fw={300} className="mv:2">
							Our team of professionals began providing international
							procurement and logistics services to the restaurant and
							hospitality industries in 2006. In 2014, to flourish relationships
							and provide customers with valuable experiences and utmost
							results, LIT opened its office in El Paso, TX, a prominent
							economic region along the US-Mexican border. As such, we have
							become the inter- national bridge to success by providing our
							partners and customers with bountiful opportuni- ties and
							flexibility.
						</Text>
						<Text fs={18} fw={300} className="mv:2">
							After years of experience and growth, we have evolved into a more
							robust enterprise. From feasibility studies and personalized
							procurement plans to complete project management and energy
							efficiency services, we are with you every step of the way. Our
							agile team offers integra- ted and engaged customer relationships
							and partnerships to obtain successful solutions in diverse
							industries.
						</Text>
					</>
				)}
				{showServices && (
					<>
						<Text fs={18} fw={300} className="mv:2">
							Our talented team converts concepts into reality by strategically
							collaborating with customers, partners, and suppliers. From
							feasibility studies and personalized procurement plans to complete
							construction project management with energy efficiency services,
							we are with you every step of the way. Successful solutions and
							optimum results are obtained through our customized methodology
							built on a systematic, timely, and controlled process. We serve a
							diverse set of customers, such as hotels, restaurants, and beyond.
						</Text>

						<Flex className="mb:2">
							<ol>
								<li>• Property Improvement Plan Administration</li>
								<li>• Scope of work definition </li>
								<li>• Compliance with brand standards</li>
								<li>• Budget development</li>
								<li>• Project and contract management </li>
								<li>
									• Procurement: Furniture, Fixtures & Equipment / Operating,
									Supplies & Equipment{" "}
								</li>
							</ol>
						</Flex>
						<Flex className="mb:2">
							<ol>
								<li>• Global sourcing </li>
								<li>• Import & export services </li>
								<li>• Logistics </li>
								<li>• Installation </li>
								<li>• Construction Project Management </li>
								<li>• Conception & Initiation</li>
								<li>• Definition & Planning </li>
								<li>• Launch & Execution </li>
							</ol>
						</Flex>
						<Flex className="mb:2">
							<ol>
								<li>• Performance & Control</li>
								<li>• Project Close </li>
								<li>• Energy Management </li>
								<li>• Feasibility studies</li>
								<li>• Operational assessments </li>
								<li>
									• Evaluation of mechanical, electrical, & plumbing systems{" "}
								</li>
								<li>• Financing solutions for energy saving initiatives</li>
							</ol>
						</Flex>
					</>
				)}
			</Flex>
		</Flex>
	)
}

export default Services
