import React from "react"
import { Text, Button, Flex } from "@components"
import { SubTitle } from "@modules"

const About = () => {
	return (
		<Flex className="pv:5 ph:2" align="center" justify="center">
			<Flex mdWidth="60%">
				<SubTitle invert fs={35}>THE LIT DIFFERENCE</SubTitle>
				<Text fs={18} fw={300} className="mv:1">
					- Transparency is key to upholding our customer and partner
					relationships.
				</Text>

				<Text fs={18} fw={300} className="mv:1">
					- Through our commitment to exceptional customer service and proven
					value-based me- thodology, we deliver premium results. We bring peace
					of mind to our customers by ensuring every project runs smoothly, on
					time, on budget, and within scope.
				</Text>
				<Text fs={18} fw={300} className="mv:1">
					- With a depth of tools and resources, our agile team is prepared to
					efficiently adapt to any arising conflicts and shifts within a given
					project.
				</Text>
				<Text fs={18} fw={300} className="mv:1">
					- Our highly qualified team of professionals is certified and
					exceptionally trained. We comply with all required certifications,
					licenses, and codes, and add value to our customers’ projects by
					harmonizing efficiency and best practices.
				</Text>
				<Text fs={18} fw={300} className="mv:1">
					- We represent our customers’ best interests and obtain optimum
					benefits for their projects from start to finish. Whether it be
					complying to brand standards or negotiating on behalf of new
					construction projects, our team is your ultimate intermediary between
					brands and suppliers.
				</Text>
				<Text fs={18} fw={300} className="mv:1">
					- Our dedication to quality lies within our engaged relationships,
					reputable network of con- tractors, diligent installations, and robust
					monitoring and evaluation system.
				</Text>
				<Text fs={18} fw={300} className="mv:1">
					- We propel your company forward by minimizing detrimental risks. Our
					risk management team quantifies the impact of every issue. and
					prepares an action plan.
				</Text>
				<Text fs={18} fw={300} className="mv:1">
					- Our team thrives on passion. We are continually learning and seeking
					improvements and advancements to enhance our business communities.
				</Text>
			</Flex>
		</Flex>
	)
}

export default About
