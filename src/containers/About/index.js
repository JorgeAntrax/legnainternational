export { default as Banner } from './Banner';
export { default as About } from './About';
export { default as Difference } from './Difference';
export { default as Services } from './Services';
