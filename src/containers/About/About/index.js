import React from "react"
import { Text, Button, Flex } from "@components"
import { SubTitle } from "@modules"

const About = () => {
	return (
		<Flex className="pv:5 ph:2" align="center" justify="center">
			<Flex mdWidth="60%">
				<SubTitle fs={26}>MISION</SubTitle>
				<Text fs={18} fw={300} className="mv:2">
					We assist our customers thrive by surpassing their project goals
					within the given constraints. We are dedicated to achieving tailored,
					flawless solutions by challenging the status quo and integrating
					efficient and versatile advancements.
				</Text>
				<SubTitle fs={26}>VISION</SubTitle>
				<Text fs={18} fw={300} className="mv:2">
					We aspire to enhance business communities by executing topnotch
					projects with environmen- tally sustainable solutions.
				</Text>
				<SubTitle fs={26}>CORE VALUES</SubTitle>

				<Flex className="mv:2">
					<ol className="mr:2">
						<li>• Integrity</li>
						<li>• Efficiency</li>
						<li>• Resilience</li>
						<li>• Performance</li>
					</ol>
					<ol className="mh:2">
						<li>• Loyalty</li>
						<li>• Authenticity</li>
						<li>• Versatility</li>
					</ol>
					<ol className="mh:2">
						<li>• Diversity</li>
						<li>• Motivation</li>
						<li>• Passion</li>
						<li>• Joy</li>
					</ol>
				</Flex>
			</Flex>
		</Flex>
	)
}

export default About
