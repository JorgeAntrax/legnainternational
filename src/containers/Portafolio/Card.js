import React from "react"
import { Flex, Text } from "@components"
import styled from "styled-components"

const Wrapper = styled.div`
	display: flex;
	background: url(${({ cover }) => cover}) rgba(0, 0, 0, 0.6) no-repeat center;
	position: relative;
	overflow: hidden;
	width: 100%;
	height: 350px;

	@media screen and (min-width: 768px) {
		height: 500px;
	}

	&:hover .content {
		transform: translateY(-100%);
		pointer-events: all;
	}
`

const Content = styled.div`
	transition: all 200ms ease;
	display: block;
	padding: 3rem;
	overflow: hidden;
	background-color: #1195cc;
	color: white;

	position: absolute;
	top: 100%;
	left: 0;
	width: 100%;
	height: 100%;
	pointer-events: none;
	z-index: 2;
`

const Line = styled.div`
	display: inline-block;
	height: 2px;
	width: 100px;
	background-color: #a7d05d;
	margin-left: 1rem;
`

const Div = styled.div`
	display: block;
	padding: 1rem;
	width: 100%;
	cursor: pointer;

	@media screen and (min-width: 768px) {
		width: ${({ width }) => width || 50}%;
	}
`

const Card = ({ data, width, onGallery }) => {
	return (
		<Div width={width} onClick={() => onGallery()}>
			<Wrapper cover={data.gallery[0]}>
				<Content className="content">
					<Flex wrapper align="center">
						<Text type="inline" width="auto" fs={35}>Procurement</Text>
						<Line />
					</Flex>
					<Text fs={26} fw={300} style={{ fontStyle: "italic" }}>
						<i>Client / {data.title || ""}</i>
					</Text>
					<Text fs={16} style={{ fontStyle: "italic" }}>
						{data.description || ""}
					</Text>
				</Content>
			</Wrapper>
		</Div>
	)
}

export default Card
