import React, { useState, useEffect } from "react"
import { Text, Flex, Grid, Viewer } from "@components"
import Card from "./Card"

import img8188 from "@assets/proyects/meetings/IMG_8188.jpg"
import img8189 from "@assets/proyects/meetings/IMG_8189.jpg"
import img8191 from "@assets/proyects/meetings/IMG_8191.jpg"
import img8193 from "@assets/proyects/meetings/IMG_8193.jpg"
import img8194 from "@assets/proyects/meetings/IMG_8194.jpg"
import img8195 from "@assets/proyects/meetings/IMG_8195.jpg"
import img8196 from "@assets/proyects/meetings/IMG_8196.jpg"

//corridor
import img8244 from "@assets/proyects/corridor/IMG_8244.jpg"
import img8249 from "@assets/proyects/corridor/IMG_8249.jpg"
//fitness
import img8198 from "@assets/proyects/fitness/IMG_8198.jpg"
import img8202 from "@assets/proyects/fitness/IMG_8202.jpg"
//bathrooms
import img8228 from "@assets/proyects/bathrooms/IMG_8228.jpg"
import img8230 from "@assets/proyects/bathrooms/IMG_8230.jpg"
import img8231 from "@assets/proyects/bathrooms/IMG_8231.jpg"
import img8234 from "@assets/proyects/bathrooms/IMG_8234.jpg"
import img8239 from "@assets/proyects/bathrooms/IMG_8239.jpg"

//guestrooms
import img8208 from "@assets/proyects/guestrooms/IMG_8208.jpg"
import img8217 from "@assets/proyects/guestrooms/IMG_8217.jpg"
import img8221 from "@assets/proyects/guestrooms/IMG_8221.jpg"
import img8222 from "@assets/proyects/guestrooms/IMG_8222.jpg"
import img8224 from "@assets/proyects/guestrooms/IMG_8224.jpg"
import img8225 from "@assets/proyects/guestrooms/IMG_8225.jpg"
import img8226 from "@assets/proyects/guestrooms/IMG_8226.jpg"

//lobby
import img8252 from "@assets/proyects/lobby/IMG_8252.jpg"
import img8251 from "@assets/proyects/lobby/IMG_8251.jpg"
import img8256 from "@assets/proyects/lobby/IMG_8256.jpg"
import img8257 from "@assets/proyects/lobby/IMG_8257.jpg"
import img8258 from "@assets/proyects/lobby/IMG_8258.jpg"
import img8259 from "@assets/proyects/lobby/IMG_8259.jpg"
import img8260 from "@assets/proyects/lobby/IMG_8260.jpg"

const proyects = [
	{
		title: "Marriot Meeting Spaces",
		gallery: [img8188, img8189, img8191, img8193, img8194, img8195, img8196],
	},
	{
		title: "Marriot Corridor",
		gallery: [img8244, img8249],
	},
	{
		title: "Marriot Fitness Center",
		gallery: [img8198, img8202],
	},
	{
		title: "Marriot Guest Bathrooms",
		gallery: [img8228, img8230, img8231, img8234, img8239],
	},
	{
		title: "Marriot Guestrooms",
		gallery: [img8208, img8217, img8221, img8222, img8224, img8225, img8226],
	},
	{
		title: "Marriot Lobby",
		gallery: [img8252, img8251, img8256, img8257, img8258, img8259, img8260],
	},
]

const Portafolio = () => {
	const [showGallery, setShowGallery] = useState(false)
	const [currentGalleryFiles, setCurrentGalleryFiles] = useState([])
	const [currentProject, setCurrentProject] = useState("")

	const handleGallery = (p, gallery) => {
		setCurrentGalleryFiles(gallery)
		setCurrentProject(p)
		setShowGallery(true)
	}
	const handleClose = () => {
		setCurrentGalleryFiles([])
		setCurrentProject("")
		setShowGallery(false)
	}
	return (
		<Grid className="mb:3">
			<Text fs={28} fw={500} className="mv:2 p:1">
				<i>All Projects for LIT</i>
			</Text>

			<Flex align="start" justify="center">
				{!!proyects.length &&
					proyects.map(project => (
						<Card
							onGallery={() => handleGallery(project.title, project.gallery)}
							width={50}
							currentImg={0}
							data={project}
						/>
					))}
			</Flex>

			<Viewer
				show={showGallery}
				files={currentGalleryFiles}
				onClose={() => handleClose()}
				name={currentProject}
			/>
		</Grid>
	)
}

export default Portafolio
