import React, { useEffect, useContext } from "react"
import { Layout } from "@layout"
import { Seo } from "@seo"
import { useWidth } from "@hooks"
import { navigate } from "gatsby"

import { Form, Slide } from "@containers/Home"

const ContactPage = () => {
	const { isMobile } = useWidth()
	return (
		<Layout>
			<div style={{ marginTop: isMobile ? "120px" : "80px" }}></div>
			<Form />
			<Slide />
		</Layout>
	)
}

export const Head = () => <Seo title="Contact Us" />
export default ContactPage
