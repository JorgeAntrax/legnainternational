import React, { useEffect, useContext } from "react"
import { Layout } from "@layout"
import { Seo } from "@seo"
import { navigate } from "gatsby"

import { Slide } from "@containers/Home"
import { Banner, About, Services, Difference } from "@containers/About"

const PageServices = () => {
	return (
		<Layout navbarTranslucent={true}>
			<Banner title="OUR SERVICES" />
			<Services showServices={true} />
			<Slide />
		</Layout>
	)
}

export const Head = () => <Seo title="Services" />
export default PageServices
