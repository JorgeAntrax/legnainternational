import React, { useEffect, useContext } from "react";
import { Seo } from "@seo"
import { Layout } from "@layout"
import { navigate } from "gatsby";

import {Banner, About, Services, Form, Slide} from "@containers/Home"

const IndexPage = () => {

	return (
		<Layout navbarTranslucent={true}>
			<Banner />
			<About />
			<Services />
			<Form />
			<Slide />
		</Layout>
	)
};

export const Head = () => <Seo />
export default IndexPage;
