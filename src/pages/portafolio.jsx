import React, { useEffect, useContext } from "react"
import { Layout } from "@layout"
import { Seo } from "@seo"
import { navigate } from "gatsby"

import { Slide } from "@containers/Home"
import { Banner } from "@containers/About"
import { Portafolio } from "@containers"

import port from "@assets/portafolio.jpg"

const PagePortafolio = () => {
	return (
		<Layout navbarTranslucent={true}>
			<Banner title="PORTAFOLIO" image={port} />
			<Portafolio />
			<Slide />
		</Layout>
	)
}

export const Head = () => <Seo title="Portafolio" />
export default PagePortafolio
