import React, { useEffect, useContext } from "react";
import { Layout } from "@layout"
import { Seo } from "@seo"
import { navigate } from "gatsby";

import {Slide} from "@containers/Home"
import {Banner, About, Services, Difference} from "@containers/About"

const PageAbout = () => {

	return (
		<Layout navbarTranslucent={true}>
			<Banner />
			<About />
			<Services />
			<Difference />
			<Slide />
		</Layout>
	)
};

export const Head = () => <Seo title="About" />
export default PageAbout;
